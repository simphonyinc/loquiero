import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'intro', pathMatch: 'full' },
  { path: '', loadChildren: './page/tabs/tabs.module#TabsPageModule' },
  { path: 'login', loadChildren: './page/login/login.module#LoginPageModule' },
  { path: 'signup', loadChildren: './page/signup/signup.module#SignupPageModule' },
  { path: 'forgot', loadChildren: './page/forgot/forgot.module#ForgotPageModule' },
  { path: 'wordpress', loadChildren: './page/ready-app/wordpress/wordpress.module#WordpressPageModule' },
  { path: 'tienda', loadChildren: './page/tienda/tienda.module#TiendaPageModule' },
  { path: 'product-single', loadChildren: './page/product-single/product-single.module#ProductSinglePageModule' },
  { path: 'perfil', loadChildren: './page/perfil/perfil.module#PerfilPageModule' },
  { path: 'terminos', loadChildren: './page/terminos/terminos.module#TerminosPageModule' },
  { path: 'wallet', loadChildren: './page/wallet/wallet.module#WalletPageModule' },
  { path: 'editar-perfil', loadChildren: './page/editar-perfil/editar-perfil.module#EditarPerfilPageModule' },
  { path: 'productos', loadChildren: './page/productos/productos.module#ProductosPageModule' },
  { path: 'soporte', loadChildren: './page/soporte/soporte.module#SoportePageModule' },
  { path: 'inicio', loadChildren: './page/inicio/inicio.module#InicioPageModule' },
  { path: 'intro', loadChildren: './page/intro/intro.module#IntroPageModule' }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
