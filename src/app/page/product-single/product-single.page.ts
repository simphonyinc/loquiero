import { Component, OnInit } from "@angular/core";
import { ModalController } from "@ionic/angular";
import { GalleryModalComponent } from "../../component/gallery/gallery-modal/gallery-modal.component";
import { ActivatedRoute, Router } from "@angular/router";
import { SocialSharing } from "@ionic-native/social-sharing/ngx";
import { File } from "@ionic-native/file/ngx";
import { Storage } from "@ionic/storage";

//Woocommerce Api
import * as WC from "woocommerce-api";

@Component({
  selector: "app-product-single",
  templateUrl: "./product-single.page.html",
  styleUrls: ["./product-single.page.scss"]
})
export class ProductSinglePage implements OnInit {
  text: "Hola, quiero compartir contigo";
  WooCommerce: any;
  product: any = {};
  title: any;
  message: string;
  price: number;

  constructor(
    public modalController: ModalController,
    private router: Router,
    private ActiveRoute: ActivatedRoute,
    private socialSharing: SocialSharing,
    private storage: Storage,
    private file: File
  ) {
    this.ActiveRoute.queryParams.subscribe(res => {
      this.product = JSON.parse(res.value);
      for (let i = 0; i < this.product.categories.length; i++) {
        this.product.categories[i] = " " + this.product.categories[i];
      }
      console.log(this.product, "Este es la data del producto");
      console.log(this.product.images), "Imagenes del producto";
    });
    // Get lo quiero custom info
    this.storage.get("coupon_info").then(data => {
      let coupon_info = JSON.parse(data);
      const code = coupon_info.coupon_code;
      this.message =
        "En Loquiero usando mi código de líder " +
        code +
        ", tienes el 35% de descuento para comprar  ";
    });
    this.title = this.product.title;
  }

  ngOnInit() {}

  viewImg(item) {
    this.presentModal(item);
  }

  async presentModal(item) {
    const modal = await this.modalController.create({
      component: GalleryModalComponent,
      componentProps: { data_gallery: item }
    });
    return await modal.present();
  }

  shareWhatsApp(productName) {
    console.log("wa share");
    this.socialSharing.canShareVia("whatsapp").then(() => {
      this.socialSharing
        .shareViaWhatsApp(
          this.message + productName,
          this.product.featured_src,
          this.product.permalink
        )
        .then(() => {
          // Success
        })
        .catch(e => {
          console.log("Error sharing via wa" + e);
          // Error!
        });
    });
  }

  shareFacebook(productName) {
    // Image or URL works
    this.socialSharing
      .shareViaFacebook(this.product.permalink)
      .then(() => {
        console.log("success fb");
      })
      .catch(e => {
        console.log("error fb" + e);
        // Error!
      });
  }

  doShare(item) {
    this.socialSharing.share(
      this.message + this.product.title,
      "Compra en Loquiero",
      null,
      this.product.permalink
    );
  }

  shareInstagram(productName) {
    //let file = await this.resolveLocalFile();
    // Image or URL works
    this.socialSharing
      .shareViaInstagram(
        this.message + productName + " " + this.product.permalink,
        this.product.featured_src
      )
      .then(() => {
        //Exito
      })
      .catch(e => {
        // Error!
      });
  }
}
