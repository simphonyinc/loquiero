import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ModalController, LoadingController } from "@ionic/angular";
import { WalletPage } from "../../page/wallet/wallet.page";
import { EditarPerfilPage } from "../editar-perfil/editar-perfil.page";
import { Storage } from "@ionic/storage";
import { AuthService } from "src/app/provider/auth/auth.service";
import { SocialSharing } from "@ionic-native/social-sharing/ngx";
import { HTTP } from "@ionic-native/http/ngx";

@Component({
  selector: "app-perfil",
  templateUrl: "./perfil.page.html",
  styleUrls: ["./perfil.page.scss"]
})
export class PerfilPage implements OnInit {
  user = {
    code: "",
    amount: 0,
    coupon_times: 0,
    username: "",
    email: ""
  };
  message = "";

  constructor(
    private router: Router,
    public modalController: ModalController,
    public authService: AuthService,
    private storage: Storage,
    private socialSharing: SocialSharing,
    public http: HTTP,
    public loadingController: LoadingController
  ) {}

  ionViewWillEnter() {
    console.log("Enter view");
    //Get user info
    this.loadingController
      .create({
        animated: true,
        spinner: "bubbles"
      })
      .then(loader => {
        loader.present();
        this.updateData().then(() => {
          loader.dismiss();
        });
      });
  }

  async updateData() {
    await this.storage.get("user_info").then(data => {
      try {
        let user_info = JSON.parse(data);
        this.user.username = user_info.user_display_name;
        this.user.email = user_info.user_email;
      } catch (error) {
        this.user.username = data.user_display_name;
        this.user.email = data.user_email;
      }
    });

    await this.storage.get("token").then(token => {
      const userInfo =
        "https://www.lo-quiero.com.co/wp-json/loquieroapi/v1/userinfo";
      this.http
        .get(userInfo, null, { Authorization: "Bearer " + token })
        .then(data => {
          this.storage.set("coupon_info", data.data);
          // Get lo quiero custom info
          this.storage.get("coupon_info").then(data_ => {
            let coupon_info = JSON.parse(data_);
            this.user.code = coupon_info.coupon_code;
            if (coupon_info.amount != "") {
              this.user.amount = coupon_info.amount;
            }
            // Message
            this.message =
              "Hola, quiero compartir contigo mi código de descuento " +
              this.user.code +
              " puedes usarlo en toda la tienda y ¡obtener descuentos del 35%!, sólo en Loquiero! ";
          });
        })
        .catch(error => {
          console.log("error_loquiero_info", error);
        });
    });
    await this.storage.get("coupon_info").then(data => {
      this.user.code = JSON.parse(data).coupon_code;
      const couponUrl =
        "https://www.lo-quiero.com.co/wp-json/loquieroapi/v1/coupons/";
      this.http
        .post(couponUrl, { coupon_code: this.user.code }, {})
        .then(data => {
          let info = JSON.parse(data.data);
          if (info.total_usaged != null) {
            this.user.coupon_times = info.total_usaged;
          }
        })
        .catch(error => {
          alert("No hay información de tu código lider");
          console.log(error);
        });
    });
  }

  ngOnInit() {}

  irFuera(): void {
    this.storage.clear();
    this.storage.set("login", false);
    this.router.navigate(["login"]);
  }

  presentWallet() {
    this.irWallet();
  }

  //Modal para ver la billetera del usuario
  async irWallet() {
    const modal = await this.modalController.create({
      component: WalletPage
    });
    modal.onDidDismiss().then(data => {
      this.updateData();
    });
    return await modal.present();
  }

  //Modal para editar info del perfil
  async irEditar() {
    const modal = await this.modalController.create({
      component: EditarPerfilPage
    });
    modal.onDidDismiss().then(data => {
      this.updateData();
    });
    return await modal.present();
  }

  async shareWhatsApp() {
    this.socialSharing.canShareVia("whatsapp").then(() => {
      this.socialSharing
        .shareViaWhatsApp(
          this.message,
          null,
          "https://www.lo-quiero.com.co/tienda/"
        )
        .then(() => {
          // Success
        })
        .catch(e => {
          console.log("Error sharing via wa" + e);
          // Error!
        });
    });
  }

  async shareFacebook() {
    // Image or URL works
    this.socialSharing
      .shareViaFacebook(this.message)
      .then(() => {
        console.log("success fb");
      })
      .catch(e => {
        console.log("error fb" + e);
        // Error!
      });
  }

  async shareInstagram() {
    // Image or URL works
    this.socialSharing
      .shareViaInstagram(
        this.message + "https://www.lo-quiero.com.co/tienda/",
        null
      )
      .then(() => {
        //Exito
      })
      .catch(e => {
        // Error!
      });
  }
}
