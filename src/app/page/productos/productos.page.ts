import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

//Woocommerce Api
import * as WC from "woocommerce-api";
import { LoadingController } from "@ionic/angular";

@Component({
  selector: "app-productos",
  templateUrl: "./productos.page.html",
  styleUrls: ["./productos.page.scss"]
})
export class ProductosPage implements OnInit {
  WooCommerce: any;
  products: any[];
  page: number;
  category: any;
  categories: any;
  categoryName: "";
  show: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public loadingController: LoadingController
  ) {
    this.page = 1;
    this.WooCommerce = WC({
      url: "http://www.lo-quiero.com.co",
      consumerKey: "ck_e12cd4956edb43e99bd8af355b1c04e4c84362e8",
      consumerSecret: "cs_926bad1a707f21fbb4d04a20e0a28110d574644c"
    });
    let paramData = JSON.parse(this.route.snapshot.paramMap.get("value"));
    this.category = paramData;
    this.categoryName = paramData.name;
    this.getData();
  }

  async getData() {
    this.loadingController
      .create({
        message: "Cargando productos...",
        animated: true,
        spinner: "bubbles"
      })
      .then(loader => {
        loader.present();
        this.WooCommerce.getAsync(
          "products?filter[category]=" + this.category.slug
        ).then(
          data => {
            let infoProducts = JSON.parse(data.body);
            this.products = infoProducts.products;
            if (this.products.length == 0) {
              console.log("willn't hide");
              this.show = true;
            }
            loader.dismiss();
            document.getElementById("loquiero").click();
          },
          err => {
            console.log(err);
          }
        );
      });
  }

  ngOnInit() {}

  irProducto(product) {
    this.router.navigate(["product-single"], {
      queryParams: {
        value: JSON.stringify(product)
      }
    });
  }
}
