import { Component, OnInit } from "@angular/core";
import { Router, NavigationExtras } from "@angular/router";
//Woocommerce Api
import * as WC from "woocommerce-api";
import { loadingController } from "@ionic/core";
import { LoadingController } from "@ionic/angular";

@Component({
  selector: "app-tienda",
  templateUrl: "./tienda.page.html",
  styleUrls: ["./tienda.page.scss"]
})
export class TiendaPage implements OnInit {
  WooCommerce: any;
  products: any[];
  categories: any[] = [];
  coupons: any[];
  spnState: string = "show";
  spnState2: string = "show";
  product: any;

  constructor(
    private router: Router,
    public loadingController: LoadingController
  ) {
    this.WooCommerce = WC({
      url: "http://www.lo-quiero.com.co",
      consumerKey: "ck_e12cd4956edb43e99bd8af355b1c04e4c84362e8",
      consumerSecret: "cs_926bad1a707f21fbb4d04a20e0a28110d574644c"
    });
  }

  async getData() {
    await this.WooCommerce.getAsync("products").then(
      data => {
        this.products = JSON.parse(data.body).products;
        this.spnState2 = "hide";
        document.getElementById("loquiero-product").click();
      },
      err => {
        console.log(err, "No se ha traido los productos");
        this.spnState2 = "hide";
      }
    );
    //Trae las categorías de los productos
    await this.WooCommerce.getAsync("products/categories").then(
      data => {
        let temp: any[] = JSON.parse(data.body).product_categories;
        for (let i = 0; i < temp.length; i++) {
          if (temp[i].parent === 0) {
            this.categories.push(temp[i]);
            this.spnState = "hide";
            document.getElementById("loquiero-category").click();
          }
        }
      },
      err => {
        console.log(err, "No se han traido las categorias");
        this.spnState = "hide";
      }
    );
  }

  irProducto(product) {
    this.router.navigate(["product-single"], {
      queryParams: {
        value: JSON.stringify(product)
      }
    });
  }

  abrirCategoria(category: any) {
    this.router.navigate([
      "/productos",
      {
        value: JSON.stringify(category)
      }
    ]);
  }

  irPerfil(): void {
    this.router.navigate(["tabs/tab-perfil"]);
  }

  ngOnInit() {
    this.loadingController
      .create({
        animated: true,
        spinner: "bubbles"
      })
      .then(loader => {
        loader.present();
        this.getData().then(() => {
          loader.dismiss();
        });
      });
  }

  ionViewWillEnter() {}
}
