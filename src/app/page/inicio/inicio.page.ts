import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

    slideFlipOpts = {
      effect: 'flip',
      speed: 1000,
      loop: true,
      autoplay: {
        delay: 500,
    },
  };

 

  constructor(private router: Router) {

    
   }

  ngOnInit() {
  }

  irTienda(): void {
    this.router.navigate(['tabs/tab-tienda']);
  }

  irNoticias(): void {
    this.router.navigate(['tabs/tab-blog']);
  }

  irPerfil(): void {
    this.router.navigate(['tabs/tab-perfil']);
  }

  irSoporte(): void {
    this.router.navigate(['tabs/tab-soporte']);
  }


 

}
