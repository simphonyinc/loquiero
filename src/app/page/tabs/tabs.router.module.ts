import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'tab-home',
        children: [
          {
            path: '',
            loadChildren: '../tab-home/tab-home.module#TabHomePageModule'
          },
          {
            path: 'blog',
            loadChildren: '../blog/blog.module#BlogPageModule'
          },
          {
            path: 'error',
            loadChildren: '../error/error.module#ErrorPageModule'
          },
          {
            path: 'wordpress',
            loadChildren: '../ready-app/wordpress/wordpress.module#WordpressPageModule'
          },
          {
            path: 'wordpress-category',
            loadChildren: '../ready-app/wordpress/wordpress-category/wordpress-category.module#WordpressCategoryPageModule'
          },
          {
            path: 'wordpress-posts',
            loadChildren: '../ready-app/wordpress/wordpress-posts/wordpress-posts.module#WordpressPostsPageModule'
          },
          {
            path: 'wordpress-detail',
            loadChildren: '../ready-app/wordpress/wordpress-detail/wordpress-detail.module#WordpressDetailPageModule'
          },
          {
            path: 'wordpress-favorite',
            loadChildren: '../ready-app/wordpress/wordpress-favorite/wordpress-favorite.module#WordpressFavoritePageModule'
          },
        ]
      },
      /*{
        path: 'tab-setting',
        children: [
          {
            path: '',
            loadChildren: '../tab-setting/tab-setting.module#TabSettingPageModule'
          }
        ]
      },*/
      {
        path: 'tab-inicio',
        children: [
          {
            path: '',
            loadChildren: '../inicio/inicio.module#InicioPageModule'
          }
        ]
      },
      {
        path: 'tab-tienda',
        children: [
          {
            path: '',
            loadChildren: '../tienda/tienda.module#TiendaPageModule'
          }
        ]
      },
      {
        path: 'tab-perfil',
        children: [
          {
            path: '',
            loadChildren: '../perfil/perfil.module#PerfilPageModule'
          }
        ]
      },
      {
        path: 'tab-soporte',
        children: [
          {
            path: '',
            loadChildren: '../soporte/soporte.module#SoportePageModule'
          }
        ]
      },
      
      {
        path: 'tab-blog',
        children: [
          {
            path: '',
            loadChildren: '../ready-app/wordpress/wordpress.module#WordpressPageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/tab-home',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/tab-home',
    pathMatch: 'full'
  },
  
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
