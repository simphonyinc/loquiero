import { Component, OnInit } from "@angular/core";
import {
  LoadingController,
  AlertController,
  ToastController
} from "@ionic/angular";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup } from "@angular/forms";
import { Storage } from "@ionic/storage";
import { HTTP } from "@ionic-native/http/ngx";
import { Facebook } from "@ionic-native/facebook/ngx";
import { AuthService } from "src/app/provider/auth/auth.service";

@Component({
  selector: "app-login",
  templateUrl: "login.page.html",
  styleUrls: ["login.page.scss"]
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  loading: any;
  model: any = {};
  data: any = {};
  passwrodType: string = "password";
  passwordShow: boolean = false;
  user: any;

  constructor(
    public alertController: AlertController,
    public loadingController: LoadingController,
    private router: Router,
    public toastCtrl: ToastController,
    public formBuilder: FormBuilder,
    public authService: AuthService,
    public http: HTTP,
    private storage: Storage,
    private facebook: Facebook
  ) {}

  public togglePassword() {
    if (this.passwordShow) {
      this.passwordShow = false;
      this.passwrodType = "password";
    } else {
      this.passwordShow = true;
      this.passwrodType = "text";
    }
  }

  ngOnInit() {}

  async presentAlert() {
    const alert = await this.alertController.create({
      header: "¡Opps!",
      message:
        "El usuario o contraseña es incorrecto, por favor intenta de nuevo",
      buttons: ["OK"]
    });
    await alert.present();
  }

  async presentToast() {
    const toast = await this.toastCtrl.create({
      message:
        "Ops! El usuario o contraseña son incorrectos, intenta de nuevo por favor.",
      color: "warning",
      animated: true,
      showCloseButton: true
    });
    await toast.present();
  }

  openSignup(): void {
    this.router.navigate(["signup"]);
  }

  openForgotPwd(): void {
    this.router.navigate(["forgot"]);
  }

  getLoquieroInfo(token: String) {
    const forgotUrl =
      "https://www.lo-quiero.com.co/wp-json/loquieroapi/v1/userinfo";
    this.http
      .get(forgotUrl, null, { Authorization: "Bearer " + token })
      .then(data => {
        this.storage.set("coupon_info", data.data);
      })
      .catch(error => {
        console.log("error_loquiero_info", error);
      });
  }

  facebookLogin() {
    this.facebook
      .login(["email"])
      .then(response => {
        if (response.status === "connected") {
          console.log("Logged in succesfully");
          this.facebookRegister(response.authResponse.accessToken);
        } else {
          console.log("Not logged in :(");
        }
      })
      .catch(error => console.log("Error logging into Facebook", error));
  }

  facebookRegister(accessToken) {
    this.loadingController.create({}).then(loader => {
      loader.present();
      const facebookAuthUrl =
        "https://www.lo-quiero.com.co/wp-json/loquieroapi/v1/facebook/login";
      this.http
        .post(
          facebookAuthUrl,
          {
            access_token: accessToken
          },
          {}
        )
        .then(data => {
          let authInfo = JSON.parse(data.data);
          this.getLoquieroInfo(authInfo.token);
          this.storage.set("token", authInfo.token);
          console.log(data.data);
          this.storage.set("user_info", data.data);
          this.storage.set("login", true);
          loader.dismiss();
          this.router.navigate(["intro"]);
        })
        .catch(error => {
          console.log("error_facebooklogin", error);
          loader.dismiss();
        });
    });
  }

  onLogin(form) {
    this.loadingController
      .create({
        message: "¡Bienvenido a Loquiero!",
        animated: true,
        spinner: "bubbles"
      })
      .then(loader => {
        loader.present();
        this.authService
          .postLogin(form.value)
          .then(data => {
            let authInfo = JSON.parse(data.data);
            console.log(data.data);
            this.getLoquieroInfo(authInfo.token);
            this.storage.set("token", authInfo.token);
            this.storage.set("user_info", data.data);
            this.storage.set("login", true);
            loader.dismiss();
            this.router.navigate(["intro"]);
          })
          .catch(err => {
            this.storage.set("login", false);
            loader.dismiss();
            this.presentToast();
          });
      });
  }
}
