import { Component, OnInit } from "@angular/core";
import { LoadingController, AlertController } from "@ionic/angular";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { HTTP } from "@ionic-native/http/ngx";

@Component({
  selector: "app-forgot",
  templateUrl: "forgot.page.html",
  styleUrls: ["forgot.page.scss"]
})
export class ForgotPage implements OnInit {
  resetPasswordForm: FormGroup;
  loading: any;
  constructor(
    public alertController: AlertController,
    public loadingCtrl: LoadingController,
    private router: Router,
    public formBuilder: FormBuilder,
    public http: HTTP
  ) {
    this.resetPasswordForm = formBuilder.group({
      email: ["", Validators.compose([Validators.required, Validators.email])]
    });
  }

  ngOnInit() {}

  async resetPassword() {
    if (!this.resetPasswordForm.valid) {
      console.log(this.resetPasswordForm.value);
    } else {
      this.loadingCtrl.create({}).then(loader => {
        loader.present();
        const forgotUrl =
          "https://www.lo-quiero.com.co/wp-json/loquieroapi/v1/password";
        this.http
          .post(
            forgotUrl,
            {
              user_login: this.resetPasswordForm.value.email
            },
            {}
          )
          .then(data => {
            this.resetPasswordForm.reset();
            loader.dismiss();
            this.presentAlertSuccess();
            this.router.navigate(["login"]);
          })
          .catch(error => {
            loader.dismiss();
            this.presentAlertError();
          });
      });
    }
  }

  backPage() {
    this.router.navigate(["login"]);
  }

  async presentAlertError() {
    const alert = await this.alertController.create({
      header: "Error",
      message: "Porfavor, inténtalo de nuevo en un momento",
      buttons: ["Ok"]
    });
    await alert.present();
  }

  async presentAlertSuccess() {
    const alert = await this.alertController.create({
      header: "Recuperar contraseña",
      message:
        "Hemos enviado un correo electrónico con las instrucciones para recuperar tu contraseña.",
      buttons: ["Ok"]
    });
    await alert.present();
  }
}
