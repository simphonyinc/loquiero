import { Component, OnInit } from "@angular/core";
import {
  ModalController,
  AlertController,
  ToastController
} from "@ionic/angular";
import { Storage } from "@ionic/storage";
import { HTTP } from "@ionic-native/http/ngx";

@Component({
  selector: "app-wallet",
  templateUrl: "./wallet.page.html",
  styleUrls: ["./wallet.page.scss"]
})
export class WalletPage implements OnInit {
  user = {
    code: "",
    amount: 0,
    coupon_times: 0,
    username: "",
    email: ""
  };

  constructor(
    public modalController: ModalController,
    private storage: Storage,
    public alertController: AlertController,
    public toastCtrl: ToastController,
    public http: HTTP
  ) {
    this.storage.get("coupon_info").then(data => {
      let coupon_info = JSON.parse(data);
      this.user.code = coupon_info.coupon_code;
      if (coupon_info.amount != "") {
        this.user.amount = coupon_info.amount;
      }
      this.storage.get("user_info").then(data => {
        try {
          let user_info = JSON.parse(data);
          this.user.username = user_info.user_display_name;
        } catch (error) {
          this.user.username = data.user_display_name;
        }
      });
    });
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: "Felicitaciones",
      message: "Se ha enviado tu solicitud y te responderemos en breve.",
      buttons: [
        {
          text: "Aceptar",
          handler: () => {
            this.dismiss();
          }
        }
      ]
    });
    await alert.present();
  }

  async presentConfirmationAlert() {
    const alert = await this.alertController.create({
      header: "¿Estás seguro?",
      message:
        "Tus ganancias volverán a ceros y nos comunicaremos contigo para entregarte tu dinero.",
      buttons: [
        {
          text: "Cancelar",
          handler: () => {
            this.dismiss();
          }
        },
        {
          text: "Aceptar",
          handler: () => {
            this.reset();
          }
        }
      ]
    });
    await alert.present();
  }

  reset() {
    if (this.user.amount == 0) {
      this.presentToast();
      return;
    }
    this.storage.get("token").then(data => {
      //let authInfo = JSON.parse(data);
      console.log(data);
      const resetUrl =
        "https://www.lo-quiero.com.co/wp-json/loquieroapi/v1/reset";
      this.http
        .get(resetUrl, null, { Authorization: "Bearer " + data })
        .then(data => {
          this.user.amount = 0;
          this.presentAlert();
        })
        .catch(error => {
          console.log(error);
          this.presentToast();
        });
    });
  }

  async presentToast() {
    const toast = await this.toastCtrl.create({
      message:
        "Aún no puedes reclamar tus ganancias, contacta con nosotros o inténtalo más tarde.",
      color: "warning",
      animated: true,
      showCloseButton: true
    });
    await toast.present();
  }

  ngOnInit() {}

  dismiss() {
    this.modalController.dismiss();
  }
}
