import { Component, OnInit } from "@angular/core";
import {
  ModalController,
  AlertController,
  LoadingController
} from "@ionic/angular";
import { HTTP } from "@ionic-native/http/ngx";
import { Storage } from "@ionic/storage";
import * as WC from "woocommerce-api";
import { WoocommerceService } from "src/app/provider/woocommerce/woocommerce.service";
import { AuthService } from "src/app/provider/auth/auth.service";

@Component({
  selector: "app-editar-perfil",
  templateUrl: "./editar-perfil.page.html",
  styleUrls: ["./editar-perfil.page.scss"]
})
export class EditarPerfilPage implements OnInit {
  user = {
    code: "",
    amount: 0,
    coupon_times: 0,
    name: "",
    lastname: "",
    email: ""
  };
  WooCommerce: any;
  constructor(
    public modalController: ModalController,
    public alertCtrl: AlertController,
    public http: HTTP,
    private storage: Storage,
    private WP: WoocommerceService,
    public loadingController: LoadingController
  ) {
    this.WooCommerce = this.WP.init();
  }

  ngOnInit() {}

  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: "¿Estás seguro?",
      message:
        "Estas apunto de cambiar la información de tu perfil, ¿estás de acuerdo?",
      buttons: [
        {
          text: "No",
          role: "cancel",
          cssClass: "secondary",
          handler: blah => {
            console.log("Se ha cancelado la edición");
          }
        },
        {
          text: "Si",
          handler: () => {
            this.updateUser();
            console.log("Se ha editado");
          }
        }
      ]
    });
    await alert.present();
  }

  updateUser() {
    this.storage.get("coupon_info").then(data => {
      console.log(data);
      let user_info = JSON.parse(data);
      let id = user_info.id;
      this.loadingController
        .create({
          message: "Estamos editando tu usuario",
          animated: true,
          spinner: "bubbles"
        })
        .then(loader => {
          loader.present();
          this.WooCommerce.putAsync("customers/" + id, {
            first_name: this.user.name,
            last_name: this.user.lastname
          })
            .then(data => {
              const response = JSON.parse(data.body);
              this.storage.set("user_info", {
                user_nicename: this.user.name,
                user_display_name: this.user.name,
                user_email: response.email
              });
              loader.dismiss();
              this.dismiss();
            })
            .catch(err => {
              console.log(err);
              loader.dismiss();
              this.dismiss();
            });
        });
    });
  }

  dismiss() {
    this.modalController.dismiss();
  }
}
