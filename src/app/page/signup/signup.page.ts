import { Component, OnInit } from "@angular/core";
import {
  LoadingController,
  AlertController,
  ToastController
} from "@ionic/angular";
import { Router } from "@angular/router";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from "@angular/forms";
// Woocommerce Api
import * as WC from "woocommerce-api";
import { WoocommerceService } from "src/app/provider/woocommerce/woocommerce.service";
import { AuthService } from "src/app/provider/auth/auth.service";

@Component({
  selector: "app-signup",
  templateUrl: "signup.page.html",
  styleUrls: ["signup.page.scss"]
})
export class SignupPage implements OnInit {
  signupForm: FormGroup;
  loading: any;
  newUser: any = {};
  WooCommerce: any;
  constructor(
    public alertController: AlertController,
    public loadingController: LoadingController,
    private router: Router,
    public formBuilder: FormBuilder,
    public toastCtrl: ToastController,
    public authService: AuthService,
    private WP: WoocommerceService
  ) {
    this.WooCommerce = this.WP.init();
  }

  /** Check if email is valid */
  async checkEmail() {
    let validEmail = false;
    const reg = /^(([^<>()[\]\\.,;:\s@']+(\.[^<>()[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (reg.test(this.newUser.email)) {
      // Email looks valid
      this.WooCommerce.getAsync("customers/email/" + this.newUser.email).then(
        data => {
          const res = JSON.parse(data.body);
          if (res.errors) {
            validEmail = true;
            console.log(validEmail, "Felicitaciones el correo esta bien");
          } else {
            validEmail = false;
          }
          console.log(validEmail, "Email is already registered");
        }
      );
    } else {
      validEmail = false;
      const toast = await this.toastCtrl.create({
        message: "Correo inválido, por favor revísalo.",
        showCloseButton: true
      });
      toast.present();
      console.log(validEmail);
    }
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: "Felicitaciones",
      message:
        "Tu cuenta ha sido creada exitosamente, por favor ingresa para continuar",
      buttons: [
        {
          text: "Entrar",
          handler: () => {
            this.router.navigate(["login"]);
          }
        }
      ]
    });
    await alert.present();
  }

  async presentToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      showCloseButton: true
    });
    await toast.present();
  }

  /** Create customer */
  signUp() {
    console.log(this.newUser.terms);
    if (this.newUser.terms) {
      let customerData = {
        email: this.newUser.email,
        first_name: this.newUser.nombre_usuario,
        last_name: this.newUser.apellido_usuario,
        password: this.newUser.contrasena_usuario,
        billing: {
          phone: this.newUser.telefono_usuario
        }
      };
      this.loadingController
        .create({
          message: "Estamos creando a tu usuario",
          animated: true,
          spinner: "bubbles"
        })
        .then(loader => {
          loader.present();
          this.WooCommerce.postAsync("customers", customerData)
            .then(data => {
              const response = JSON.parse(data.body);
              console.log(data.body);
              loader.dismiss();
              if (response.code == "rest_missing_callback_param") {
                this.presentToast(
                  "Ocurrió un error creando tu usuario, revisa todos los campos"
                );
              } else {
                if (response.code == "registration-error-email-exists") {
                  this.presentToast(
                    "Este email ya está registrado, inicia sesión en tu cuenta."
                  );
                } else {
                  this.presentAlert();
                }
              }
            })
            .catch(() => {
              loader.dismiss();
              this.presentToast(
                "Ocurrió un error creando tu usuario, revisa todos los campos"
              );
            });
        });
    } else {
      this.presentToast(
        "Debes aceptar los términos y condiciones antes de continuar."
      );
    }
  }

  ngOnInit() {}

  irTerminos() {
    this.router.navigate(["terminos"]);
  }

  backPage() {
    this.router.navigate(["login"]);
  }
}
