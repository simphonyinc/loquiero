import { Injectable } from "@angular/core";

import * as WC from "woocommerce-api";

@Injectable({
  providedIn: "root"
})
export class WoocommerceService {
  WooCommerce: any;
  WoocommerceV2: any;

  constructor() {
    this.WooCommerce = WC({
      url: "https://www.lo-quiero.com.co",
      consumerKey: "ck_e12cd4956edb43e99bd8af355b1c04e4c84362e8",
      consumerSecret: "cs_926bad1a707f21fbb4d04a20e0a28110d574644c",
      wpAPI: true,
      queryStringAuth: true,
      version: "wc/v2"
    });

    this.WoocommerceV2 = WC({
      url: "http://www.lo-quiero.com.co",
      consumerKey: "ck_2ec7be12c0191a8a222cb85e89eea6291e0746e1",
      consumerSecret: "cs_a300ce55d107c61910208ae1d37f4c4978561b87",
      wpAPI: true,
      version: "wc/v2"
    });
  }

  init(v2?: boolean) {
    if (v2 == true) {
      return this.WoocommerceV2;
    } else {
      return this.WooCommerce;
    }
  }
}
