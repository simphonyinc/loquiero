import { Injectable } from "@angular/core";
import { environment } from "../../../environments/environment";
import { HTTP } from "@ionic-native/http/ngx";
const apiUrl = environment.api_link;

@Injectable({
  providedIn: "root"
})
export class AuthService {
  constructor(public http: HTTP) {}

  postLogin(data) {
    return this.http.post(`${apiUrl}/jwt-auth/v1/token`, data, {});
  }
}
