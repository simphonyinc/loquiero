<?php
require_once '/home3/loquiero/public_html/wp-content/plugins/jwt-authentication-for-wp-rest-api/includes/vendor/firebase/php-jwt/src/BeforeValidException.php';
require_once '/home3/loquiero/public_html/wp-content/plugins/jwt-authentication-for-wp-rest-api/includes/vendor/firebase/php-jwt/src/ExpiredException.php';
require_once '/home3/loquiero/public_html/wp-content/plugins/jwt-authentication-for-wp-rest-api/includes/vendor/firebase/php-jwt/src/SignatureInvalidException.php';
require_once '/home3/loquiero/public_html/wp-content/plugins/jwt-authentication-for-wp-rest-api/includes/vendor/firebase/php-jwt/src/JWT.php';

use \Firebase\JWT\JWT;

// Do not allow directly accessing this file.
if (!defined('ABSPATH')) {
  exit('Direct script access denied.');
}

/**
 * Include the main theme class.
 */
include_once get_template_directory() . '/inc/class-g5plus-theme.php';

/**
 * Add cors header
 */
function add_cors_http_header()
{
  header("Access-Control-Allow-Origin: *");
}
add_action('init', 'add_cors_http_header'); // Hook

/** Camilo Ortiz dirty piece of code */
/*Función para el control del excerpt*/
function custom_excerpt_length($length)
{
  return 20;
}
add_filter('excerpt_length', 'custom_excerpt_length', 999);
/** Camilo Ortiz piece of code */

/** Creates custom coupon when user is registered */
function create_coupon($user_id)
{
  $coupon_code = 'LQ' . str_replace("=", "", base64_encode($user_id)) . '3UG'; // Code
  $amount = '35'; // Amount
  $discount_type = 'percent'; // Type: fixed_cart, percent, fixed_product, percent_product
  $coupon = array(
    'post_title' => $coupon_code,
    'post_content' => '',
    'post_status' => 'publish',
    'post_author' => 1,
    'post_type' => 'shop_coupon'
  );
  update_user_meta($user_id, 'personal_coupon', $coupon_code);
  $new_coupon_id = wp_insert_post($coupon);
  // Add meta
  update_post_meta($new_coupon_id, 'discount_type', $discount_type);
  update_post_meta($new_coupon_id, 'coupon_amount', $amount);
  update_post_meta($new_coupon_id, 'individual_use', 'no');
  update_post_meta($new_coupon_id, 'product_ids', '');
  update_post_meta($new_coupon_id, 'exclude_product_ids', '');
  update_post_meta($new_coupon_id, 'usage_limit', '');
  update_post_meta($new_coupon_id, 'expiry_date', '');
  update_post_meta($new_coupon_id, 'apply_before_tax', 'yes');
  update_post_meta($new_coupon_id, 'free_shipping', 'no');
}
add_action('user_register', 'create_coupon'); // Hook

/**
 * Show custom profile fields
 */
function my_show_extra_profile_fields($user)
{
  ?>
  <h3>Información adicional</h3>
  <table class="form-table">
    <tr>
      <th><label for="personal_coupon">Codigo cupón Lo-quiero</label></th>
      <td>
        <input type="text" name="personal_coupon" id="personal_coupon" value="<?php echo esc_attr(get_the_author_meta('personal_coupon', $user->ID)); ?>" class="regular-text" /><br />
        <span class="description">Cupon personalizado del usuario.</span>
      </td>
      <td>
        <input type="text" name="refered_value" id="refered_value" value="<?php echo number_format(esc_attr(get_the_author_meta('refered_value', $user->ID))); ?>" class="regular-text" /><br />
        <span class="description">Valor en comisión del usuario.</span>
      </td>
    </tr>
  </table>
<?php
}
add_action('show_user_profile', 'my_show_extra_profile_fields'); // Hook
add_action('edit_user_profile', 'my_show_extra_profile_fields'); // Hook

/**Custom rest api calls */
add_action('rest_api_init', function () {
  register_rest_route('loquieroapi/v1', 'coupons/', array(
    'methods'  => 'POST',
    'callback' => 'get_coupon_data'
  ));
  register_rest_route('loquieroapi/v1', 'password/', array(
    'methods'  => 'POST',
    'callback' => 'get_lost_password'
  ));
  register_rest_route('loquieroapi/v1', 'facebook/login', array(
    'methods'  => 'POST',
    'callback' => 'facebook_login'
  ));
  register_rest_route('loquieroapi/v1', 'userinfo', array(
    'methods' => 'GET',
    'callback' => 'loquiero_user_info'
  ));
  register_rest_route('loquieroapi/v1', 'reset', array(
    'methods' => 'GET',
    'callback' => 'reset_amount'
  ));
});

/** Facebook login, getting access_token */
function facebook_login($request)
{
  global $json_api;
  if ($json_api->query->fields) {
    $fields = $json_api->query->fields;
  } else {
    $fields = 'id,name,first_name,last_name,email';
  }
  if ($json_api->query->ssl) {
    $enable_ssl = $json_api->query->ssl;
  } else $enable_ssl = true;
  $access_token = $request['access_token'];
  if (!$access_token) {
    $json_api->error("You must include a 'access_token' variable. Get the valid access_token for this app from Facebook API.");
  } else {
    $url = 'https://graph.facebook.com/me/?fields=' . $fields . '&access_token=' . $access_token;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $enable_ssl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    $result = curl_exec($ch);
    curl_close($ch);
    $result = json_decode($result, true);
    if (isset($result["email"])) {
      $user_email = $result["email"];
      $email_exists = email_exists($user_email);
      if ($email_exists) {
        $user = get_user_by('email', $user_email);
        $user_id = $user->ID;
        $user_name = $user->user_login;
      }
      if (!$user_id && $email_exists == false) {
        $user_name = strtolower($result['first_name'] . '.' . $result['last_name']);
        while (username_exists($user_name)) {
          $user_name = strtolower($result['first_name'] . '.' . $result['last_name']);
        }
        $random_password = wp_generate_password($length = 12, $include_standard_special_chars = false);
        $userdata = array(
          'user_login' => $user_name,
          'user_email' => $user_email,
          'user_pass' => $random_password,
          'display_name' => $result["name"],
          'first_name' => $result['first_name'],
          'last_name' => $result['last_name'],
          'role' => 'customer'
        );
        $user_id = wp_insert_user($userdata);
        if ($user_id) $user_account = 'user registered.';
      } else {
        if ($user_id) $user_account = 'user logged in.';
      }
      $expiration = time() + apply_filters('auth_cookie_expiration', 1209600, $user_id, true);
      $cookie = wp_generate_auth_cookie($user_id, $expiration, 'logged_in');
      $response['msg'] = $user_account;
      $response['wp_user_id'] = $user_id;
      $response['cookie'] = $cookie;
      $response['user_login'] = $user_name;
      return generate_token(get_user_by('id', $user_id));
    } else {
      $response['msg'] = "Your 'access_token' did not return email of the user. Without 'email' user can't be logged in or registered. Get user email extended permission while joining the Facebook app.";
    }
  }
  //return $response;
}

/**
 * Generate token for JWT authentication of users
 * Also facebook
 */
function generate_token($user)
{
  $secret_key = defined('JWT_AUTH_SECRET_KEY') ? JWT_AUTH_SECRET_KEY : false;
  /** First thing, check the secret key if not exist return a error*/
  if (!$secret_key) {
    return new WP_Error(
      'jwt_auth_bad_config',
      __('JWT is not configurated properly, please contact the admin', 'wp-api-jwt-auth'),
      array(
        'status' => 403,
      )
    );
  }
  /** If the authentication fails return a error*/
  if (is_wp_error($user)) {
    $error_code = $user->get_error_code();
    return new WP_Error(
      '[jwt_auth] ' . $error_code,
      $user->get_error_message($error_code),
      array(
        'status' => 403,
      )
    );
  }
  /** Valid credentials, the user exists create the according Token */
  $issuedAt = time();
  $notBefore = apply_filters('jwt_auth_not_before', $issuedAt, $issuedAt);
  $expire = apply_filters('jwt_auth_expire', $issuedAt + (DAY_IN_SECONDS * 7), $issuedAt);
  $token = array(
    'iss' => get_bloginfo('url'),
    'iat' => $issuedAt,
    'nbf' => $notBefore,
    'exp' => $expire,
    'data' => array(
      'user' => array(
        'id' => $user->data->ID,
      ),
    ),
  );
  /** Let the user modify the token data before the sign. */
  $token = JWT::encode(apply_filters('jwt_auth_token_before_sign', $token, $user), $secret_key);
  /** The token is signed, now create the object with no sensible user data to the client*/
  $data = array(
    'token' => $token,
    'user_email' => $user->data->user_email,
    'user_nicename' => $user->data->user_nicename,
    'user_display_name' => $user->data->display_name,
  );
  /** Let the user modify the data before send it back */
  return apply_filters('jwt_auth_token_before_dispatch', $data, $user);
}

/** Set display name */
add_action('admin_head', 'make_display_name_f_name_last_name');
function make_display_name_f_name_last_name()
{
  $users = get_users(array('fields' => 'all'));
  foreach ($users as $user) {
    $user = get_userdata($user->ID);
    $display_name = $user->first_name;
    if ($display_name != ' ') wp_update_user(array('ID' => $user->ID, 'display_name' => $display_name));
    else wp_update_user(array('ID' => $user->ID, 'display_name' => $user->display_login));
    if ($user->display_name == '')
      wp_update_user(array('ID' => $user->ID, 'display_name' => $user->display_login));
  }
} // Hook 
/**
 * Get custom info of user
 */
function loquiero_user_info()
{
  $currentuserid_fromjwt = get_current_user_id();
  $data = ['id' => $currentuserid_fromjwt, 'coupon_code' => get_user_meta($currentuserid_fromjwt, 'personal_coupon', true), 'amount' => get_user_meta($currentuserid_fromjwt, 'refered_value', true)];
  $response = new WP_REST_Response($data);
  $response->set_status(200);
  return $response;
}


/** 
 * Get amount of user
 * */
function reset_amount($request)
{
  $currentuserid_fromjwt = get_current_user_id();
  $customer = get_user_by('id', $currentuserid_fromjwt);
  $amount = get_user_meta($currentuserid_fromjwt, 'refered_value', true);
  update_user_meta($currentuserid_fromjwt, 'refered_value', 0);
  // Admin email
  //$user_email = 'josemiguelchacon@outlook.com';
  $user_email = 'contactolideres@lo-quiero.com.co';
  // Redefining user_login ensures we return the right case in the email

  $message = __('Hola, hemos recibido una solicitud para obtener el dinero de los créditos del siguiente usuario:') . "\r\n\r\n";
  $message .= sprintf(__('Nombre: %s'), $customer->display_name) . "\r\n\r\n";
  $message .= sprintf(__('Email: %s'), $customer->user_email) . "\r\n\r\n";
  $message .= sprintf(__('No. de teléfono: %s'), get_user_meta($currentuserid_fromjwt, 'billing_phone', true)) . "\r\n\r\n";
  $message .= __('Valor de las ganancias: $ ' . number_format($amount)) . "\r\n\r\n";

  $message .= __('Por favor contacta con el usuario, en este momento su cuenta ha quedado en cero.') . "\r\n\r\n";
  $message .= __('Desarrollado por Simphony, INC.') . "\r\n\r\n";
  // If loquiero becomes multisite, change name of current blog
  if (is_multisite())
    $blogname = $GLOBALS['current_site']->site_name;
  else
    $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
  $title = sprintf(__('[%s] Redencion de puntos'), $blogname);
  $title = apply_filters('retrieve_password_title', $title);
  $message = apply_filters('retrieve_password_message', $message, '');
  // Check capability for send email
  if ($message && !wp_mail($user_email, $title, $message))
    new WP_Error('error', "The e-mail could not be sent. Possible reason: your host may have disabled the mail() function...", array('status' => 404));
  else
    return array(
      "msg" => 'Link for password reset has been emailed to you. Please check your email.',
    );
}

/** 
 * This easy task tooks me long time, please give him some love <3
 * Recovers user password sending email
 * */
function get_lost_password($request)
{
  global $wpdb, $wp_hasher;

  $user_login = $request['user_login'];
  if (strpos($user_login, '@')) {
    $user_data = get_user_by('email', trim($user_login));
    if (empty($user_data))
      new WP_Error("error", 'Your email address not found!', array('status' => 404));
  } else {
    $login = trim($user_login);
    $user_data = get_user_by('login', $login);
  }

  // Redefining user_login ensures we return the right case in the email
  $user_login = $user_data->user_login;
  $user_email = $user_data->user_email;
  do_action('retrieve_password', $user_login);
  $allow = apply_filters('allow_password_reset', true, $user_data->ID);
  if (!$allow) new WP_Error('not_enabled_option', 'there is no password reset option enable', array('status' => 404));
  elseif (is_wp_error($allow)) new WP_Error('error', 'error', array('status' => 404));

  $key = wp_generate_password(20, false);
  do_action('retrieve_password_key', $user_login, $key);

  if (empty($wp_hasher)) {
    require_once ABSPATH . 'wp-includes/class-phpass.php';
    $wp_hasher = new PasswordHash(8, true);
  }
  $hashed = time() . ':' . $wp_hasher->HashPassword($key);
  $wpdb->update($wpdb->users, array('user_activation_key' => $hashed), array('user_login' => $user_login));
  $message = __('Recibimos una solicitud para recuperar tu contraseña:') . "\r\n\r\n";
  $message .= network_home_url('/') . "\r\n\r\n";
  $message .= sprintf(__('Username: %s'), $user_login) . "\r\n\r\n";
  $message .= __('If this was a mistake, just ignore this email and nothing will happen.') . "\r\n\r\n";
  $message .= __('To reset your password, visit the following address:') . "\r\n\r\n";
  $message .= '<' . network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user_login), 'login') . ">\r\n";

  // If loquiero becomes multisite, change name of current blog
  if (is_multisite())
    $blogname = $GLOBALS['current_site']->site_name;
  else
    $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
  $title = sprintf(__('[%s] Password Reset'), $blogname);
  $title = apply_filters('retrieve_password_title', $title);
  $message = apply_filters('retrieve_password_message', $message, $key);
  // Check capability for send email
  if ($message && !wp_mail($user_email, $title, $message))
    new WP_Error('error', "The e-mail could not be sent. Possible reason: your host may have disabled the mail() function...", array('status' => 404));
  else
    return array(
      "msg" => 'Link for password reset has been emailed to you. Please check your email.',
    );
}

/** Get total usage for a coupon string */
function get_coupon_data($request)
{
  global $wpdb;
  $results = $wpdb->get_results("SELECT Max(CASE WHEN pm.meta_key = 'usage_count'    AND  p.`ID` = pm.`post_id` THEN pm.`meta_value` END) AS total_usaged
		FROM   `wpdl_posts` AS p 
			INNER JOIN `wpdl_postmeta` AS pm ON  p.`ID` = pm.`post_id` 
		WHERE `post_name` = '" . $request['coupon_code'] . "'
			AND p.`post_status` = 'publish' 
		GROUP  BY p.`ID` 
		ORDER  BY p.`ID` ASC", OBJECT);
  if (empty($results)) {
    return new WP_Error('not_found_coupon', 'there is no coupons with this code', array('status' => 404));
  }
  $response = new WP_REST_Response($results[0]);
  $response->set_status(200);
  return $response;
}
function loquiero_pending($order_id)
{
  error_log("$order_id set to PENDING");
}
function loquiero_failed($order_id)
{
  error_log("$order_id set to FAILED");
}
function loquiero_hold($order_id)
{
  error_log("$order_id set to ON HOLD");
}
function loquiero_processing($order_id)
{
  error_log("$order_id set to PROCESSING");
}
/** 
 * When a purchase is completed, by the payment gateway or administradtor, the coupon value is added to user
 */
function loquiero_completed($order_id)
{
  $order = wc_get_order($order_id);
  $user = new WP_User_Query(
    array(
      'fields'     => 'id',
      'meta_query' => array(
        'relation' => 'OR',
        array(
          'key'     => 'personal_coupon',
          'value'   => $order->get_used_coupons()[0],
          'compare' => '='
        )
      )

    )
  );
  $current_amount = get_user_meta($user->get_results()[0], 'refered_value');
  $new_amount = (float) $current_amount[0] + ($order->get_total() * 0.35);
  update_user_meta($user->get_results()[0], 'refered_value', $new_amount);
  error_log("$order_id set to COMPLETED");
}
/** 
 * When a purchase is refunded, then the value of personal coupon is returned 
 */
function loquiero_refunded($order_id)
{
  $order = wc_get_order($order_id);
  $user = new WP_User_Query(
    array(
      'fields'     => 'id',
      'meta_query' => array(
        'relation' => 'OR',
        array(
          'key'     => 'personal_coupon',
          'value'   => $order->get_used_coupons()[0],
          'compare' => '='
        )
      )

    )
  );
  $current_amount = get_user_meta($user->get_results()[0], 'refered_value');
  $new_amount = (float) $current_amount[0] - ($order->get_total() * 0.35);
  update_user_meta($user->get_results()[0], 'refered_value', $new_amount);
  error_log("$order_id set to REFUNDED");
}
function loquiero_cancelled($order_id)
{
  error_log("$order_id set to CANCELLED");
}
add_action('woocommerce_order_status_pending', 'loquiero_pending', 10, 1);
add_action('woocommerce_order_status_failed', 'loquiero_failed', 10, 1);
add_action('woocommerce_order_status_on-hold', 'loquiero_hold', 10, 1);
add_action('woocommerce_order_status_processing', 'loquiero_processing', 10, 1);
add_action('woocommerce_order_status_completed', 'loquiero_completed', 10, 1);
add_action('woocommerce_order_status_refunded', 'loquiero_refunded', 10, 1);
add_action('woocommerce_order_status_cancelled', 'loquiero_cancelled', 10, 1);
